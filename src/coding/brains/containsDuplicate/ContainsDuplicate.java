package src.coding.brains.containsDuplicate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class ContainsDuplicate {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter size of the array: ");
        int size = Integer.parseInt(bufferedReader.readLine());

        int[] nums = new int[size];
        System.out.println("Enter the elements in the array ->");

        for(int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            nums[i] = Integer.parseInt(bufferedReader.readLine());
        }

        bufferedReader.close();

        System.out.println("Result: " + containsDuplicate(nums));
    }

    private static boolean containsDuplicate(int[] nums) {

        Set<Integer> numSet = new HashSet<>();

        for(int element : nums) {
            if(numSet.contains(element)) return true;
            numSet.add(element);
        }

        return false;
    }
}
