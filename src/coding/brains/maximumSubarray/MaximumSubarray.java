package src.coding.brains.maximumSubarray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MaximumSubarray {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter size of the array: ");
        int size = Integer.parseInt(bufferedReader.readLine());

        int[] nums = new int[size];
        System.out.println("Enter the elements in the array ->");

        for(int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            nums[i] = Integer.parseInt(bufferedReader.readLine());
        }

        bufferedReader.close();

        System.out.println("Result: " + maxSubarray(nums));
    }

    private static int maxSubarray(int[] nums) {

        int maxSoFar = nums[0];
        int currentMax = nums[0];

        for(int i = 1; i < nums.length; i++) {
            currentMax = Math.max(nums[i], currentMax + nums[i]);
            maxSoFar = Math.max(maxSoFar, currentMax);
        }

        return maxSoFar;
    }
}
